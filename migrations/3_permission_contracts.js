var Eternal = artifacts.require("EternalStorage");
var Devotion = artifacts.require("Devotion");

module.exports = async function(deployer) {
  var e = await Eternal.deployed();
  var d = await Devotion.deployed();
  e.setAllowed(d.address);
  e.setAddress(0, process.env.ETERNAL_FEE_ADDRESS);
  d.resync();
};
