# @dev Dragonchain Devotion Contract
# @author Lance Rogers lance@dragonchain.com
from vyper.interfaces import ERC20


# External Contract Definitions
contract EternalStorage:
    def getAllowedStatus(sender: address) -> uint256: constant
    def getAddress(i: uint256) -> address: constant

# Events

## OWNABLE
OwnershipRenounced: event({_previousOwner: indexed(address)})
OwnershipTransferred: event({
    _previousOwner: indexed(address), 
    _newOwner: indexed(address)
   })

## Devotion
Devotion: event({
    _accnt: indexed(address), 
    _txnid: indexed(bytes32),
    _drgns: uint256,
    _time: uint256,
    _data: bytes32
    })
TokensReturned: event({
    _accnt: indexed(address), 
    _drgns: uint256, 
    _fee_adres: address,
    _fee: uint256
    })

# Global Declarations
token: ERC20
eternal: EternalStorage
owner: public(address)
devotionCount: public(uint256)
feeAddr: public(address)

@public
def __init__(_eternalAddress: address, _tokenContract: address):
    self.eternal = EternalStorage(_eternalAddress)
    self.token = ERC20(_tokenContract) 
    self.owner = msg.sender


#OWNABLE
# This feature is ported from Open Zeppelin. 
# The ownable feature provides basic authorization control functions 
# and simplifies the implementation of "user permissions".

@public
def renounceOwnership():
    """
    @dev Allows the current owner to relinquish control of the contract.
    @notice Renouncing ownership will leave the contract without an owner.
    It will not be possible to call the functions with the `onlyOwner`
    modifier anymore.
    """

    assert msg.sender == self.owner, "Access is denied."

    log.OwnershipRenounced(msg.sender)
    self.owner = ZERO_ADDRESS


@public 
def transferOwnership(_newOwner: address):
    """
    @dev Allows the current owner to transfer control of the contract to a newOwner.
    @param _newOwner The address to transfer ownership to.
    """
    assert msg.sender == self.owner, "Access is denied."
    assert _newOwner != ZERO_ADDRESS, "Invalid owner supplied."

    log.OwnershipTransferred(msg.sender, _newOwner)
    self.owner = _newOwner

# Devotion 
#  Manages Devotions
#  Returns Tokens to Devotees

@public
def resync():
    """
    @dev Resync fee address with EternalStorage
    """
    assert msg.sender == self.owner, "Sender not allowed"

    fee_address: address = self.eternal.getAddress(0)

    # Assert that the fee address is set in the Eternal Storage contract 
    assert fee_address != ZERO_ADDRESS, "Fee address is not registered"

    self.feeAddr = fee_address


@private
def returnToSender(_to: address, _amt: uint256, _fee: uint256):
    """
    @dev Sends DRGN that were sent from the contract address back to the sender
    @param _to Address to return DRGNs to minus the fee
    @param _amt Amount of drgns to send
    @param _fee The fee to collect
    """

    # Assert that the fee being charged is less than or equal to
    # the total amount of coins being returned
    assert _fee <= _amt, "Fee is too high"
    
    return_coins: uint256 = _amt - _fee
    self.token.transfer( _to, as_unitless_number(return_coins))
    self.token.transfer(self.feeAddr, as_unitless_number(_fee))

    log.TokensReturned(_to, _amt, self.feeAddr, _fee)


@public
def registerDevotion(_addr: address, _txnid: bytes32,\
_drgns: uint256, _time: uint256, _fee: uint256, _data: bytes32):
    """
    @dev Registers devotion and triggers returnToSender
    @param _addr The devotee account
    @param _txnid The transaction id of the sent devotion
    @param _drgns The number of dragons being devoted
    @param _time The amount of time burned as part of the devotion
    @param _fee The fee to charge
    @param _data The devotion identifier set to "0x" if None
    """

    assert self.eternal.getAllowedStatus(msg.sender) != 0, "Sender not allowed"
     
    self.returnToSender(_addr, _drgns, _fee)

    self.devotionCount += 1

    log.Devotion(_addr, _txnid, _drgns, _time, _data)


@public
def returnTokens(_addr: address, _amt: uint256, _fee: uint256):
    """
    @dev Sends DRGN that were sent from the contract address back to the sender
    @param _to Address to return DRGNs to minus the fee
    @param _amt Amount of drgns to send
    @param _fee The fee to collect
    """
    self.returnToSender(_addr, _amt, _fee)
