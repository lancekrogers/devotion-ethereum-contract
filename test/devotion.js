const Devotion = artifacts.require("Devotion");

contract("Devotion", () => {
  it("...should return tokens to sender.", async () => {
    const devotion = await Devotion.deployed();

    // Register devotion 
    await devotion.registerDevotion("0x0E8E1B8dbbdd2bF56e8dCC6dC88F2C8c8A0F2A48", 1000, 40);

    // Get devotion value
    const devoted = await devotion.devoted("0x0E8E1B8dbbdd2bF56e8dCC6dC88F2C8c8A0F2A48");

    assert.isOk(devoted);
  });
});

