# Devotion Ethereum Contract

## Eternal Storage

- Maintain list of permissioned addresses
- Maintain address to send fees too
- Run identify to add allowed addresses to the Eternal contract


## Eternal storage should be deployed first with the following arguments:
- _a -- Used to set the first allowed invoker address 
- _f -- Used to set the account where fees should be sent 


## Devotion

- Return DRGN To Sender
- Maintain list of devotions 
- devotions are stored like so:
```
transaction_id
    ├── account
    ├── drgns
    └── time
```

registerDevotion can accept an optional data field _data. This field is meant
to be used as a unique devotion identifier. If this field is not used you must
pass in "0x" in the _data field.
 


## Notes

1. DRGN tokens mainnet contract address 0x419c4db4b9e25d6db2ad9691ccb832c8d9fda05e
2. DRGN ropsten testnet contract address 0xb5fbb1a74fefc3054f4555a15e5ef66aed315bfb
3. data and transaction fields must be in 32 byte formats. The below function
   can be used to convert ethereum transaction to 32 byte arrays in python. Hex strings can also be passed into each method as long as the start with "0x"
```python
def ethereum_trxn_hash_to_bytes(hexhash): 
    return bytes.fromhex(hexhash[2:]) 
```

## Ropsten testnet
Devotion Contract:
https://ropsten.etherscan.io/address/0x4f853e060e23252caa4e5edcf7f6fbbed1ccaf36

Eternal Storage Contract:
https://ropsten.etherscan.io/address/0xddfa61cc2cf0b45f87362f5609334e1acaf5c913

## TODO

1. Fallback method that can recover non DRGN coins from  the contract if 
 invoked by an approved address (stretch goal)
