pragma solidity >=0.4.21 <0.6.0;


/**
 * @title Ownable
 * @dev The Ownable contract has an owner address, and provides basic authorization control
 * functions, this simplifies the implementation of "user permissions".
 */
contract Ownable {
    address public owner;


    event OwnershipRenounced(address indexed previousOwner);
    event OwnershipTransferred(
        address indexed previousOwner,
        address indexed newOwner
    );


    /**
     * @dev The Ownable constructor sets the original `owner` of the contract to the sender
     * account.
     */
    constructor() public {
        owner = msg.sender;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }

    /**
     * @dev Allows the current owner to relinquish control of the contract.
     * @notice Renouncing to ownership will leave the contract without an owner.
     * It will not be possible to call the functions with the `onlyOwner`
     * modifier anymore.
     */
    function renounceOwnership() public onlyOwner {
        emit OwnershipRenounced(owner);
        owner = address(0);
    }

    /**
     * @dev Allows the current owner to transfer control of the contract to a newOwner.
     * @param _newOwner The address to transfer ownership to.
     */
    function transferOwnership(address _newOwner) public onlyOwner {
        _transferOwnership(_newOwner);
    }

    /**
     * @dev Transfers control of the contract to a newOwner.
     * @param _newOwner The address to transfer ownership to.
     */
    function _transferOwnership(address _newOwner) internal {
        require(_newOwner != address(0));
        emit OwnershipTransferred(owner, _newOwner);
        owner = _newOwner;
    }
}


contract EternalStorage is Ownable {

    struct Storage {
        mapping(uint256 => uint256) _uint;
        mapping(uint256 => address) _address;
        mapping(address => uint256) _allowed;
    }

    Storage internal s;

    /**
     * @dev _unit -- Any integer 
     * @dev _address  -- Mapping of addresses to send fees 
     * @dev _allowed -- List of allowed addresses 
    **/

    constructor(
        address _a,
	address _f
        )

    public {
        setAllowed(_a);
	setAddress(0, _f);
        }

    modifier onlyAllowed() {
        require(msg.sender == owner || s._allowed[msg.sender] == uint256(1));
        _;
    }

    function setAllowed(address _address) public onlyOwner {
        s._allowed[_address] = uint256(1);
    }

    function revoke(address _address) external onlyOwner {
        s._allowed[_address] = uint256(0);
    }

    /**
     * @dev Allows the current owner to transfer control of the contract to a
     * newOwner.
     * @param newOwner The address to transfer ownership to.
     */
    function transferOwnership(address newOwner) public onlyOwner {
        Ownable.transferOwnership(newOwner);
    }

    /**
     * @dev Allows the owner to set a value for an unsigned integer variable.
     * @param i Unsigned integer variable key
     * @param v The value to be stored
     */
    function setUint(uint256 i, uint256 v) public onlyOwner {
        s._uint[i] = v;
    }

    /**
     * @dev Allows the owner to set a value for a address variable.
     * @param i Unsigned integer variable key
     * @param v The value to be stored
     */
    function setAddress(uint256 i, address v) public onlyOwner {
        s._address[i] = v;
    }

    /**
     * @dev Get the value stored of a uint variable by the hash name
     * @param i Unsigned integer variable key
     */
    function getUint(uint256 i) external view onlyAllowed returns (uint256) {
        return s._uint[i];
    }

    /**
     * @dev Get the value stored of a address variable by the hash name
     * @param i Unsigned integer variable key
     */
    function getAddress(uint256 i) external view onlyAllowed returns (address) {
        return s._address[i];
    }

    function getAllowedStatus(address a) external view onlyAllowed returns (uint) {
        return s._allowed[a];
    }

    function selfDestruct () external onlyOwner {
        selfdestruct(msg.sender);
    }
}
