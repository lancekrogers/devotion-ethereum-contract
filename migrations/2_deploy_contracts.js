var Eternal = artifacts.require("EternalStorage");
var Devotion = artifacts.require("Devotion");

module.exports = function(deployer) {
  deployer.deploy(
    Eternal,
    process.env.ETERNAL_ALLOWED_INVOKER, 
    process.env.ETERNAL_FEE_ADDRESS)
    .then(_instance =>
      deployer.deploy(
        Devotion,
        _instance.address,
        process.env.TOKEN_CONTRACT
    ));
};
